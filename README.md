## Development Stack

## Docker and Docker Compose 
* Must have docker and docker-compose installed
	- https://docs.docker.com/install/linux/docker-ce/ubuntu/
	- https://docs.docker.com/compose/install/

### Running the project
* Execute bash command ( ./run.sh )
* Visit ( http://localhost/?q=0 ) in Browser

### Coded by
* Traquena, Aries Jay C.

 
 

