<?php

require 'vendor/autoload.php';
 
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

$container = new \Slim\Container();

$container['pdo'] = function ($c) {
    $host = 'db';
    $dbname = 'au_locations';
    $username = 'api_user'; 
    $password = 'jollyhotdog-87000';
    $charset = 'utf8';
    $collate = 'utf8_unicode_ci';
    $dsn = "mysql:host=$host;dbname=$dbname;charset=$charset";
    $options = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_PERSISTENT => false,
        PDO::ATTR_EMULATE_PREPARES => false,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES $charset COLLATE $collate"
    ];

    return new PDO($dsn, $username, $password, $options);
};

$container['notFoundHandler'] = function ($c) {
    return function ($request, $response) use ($c) {
        return $response->withStatus(404)
                        ->withHeader('Content-Type', 'text/html')
                        ->write('Page not found');
    };
};
  
$app = new \Slim\App($container);
 

$app->get('/', function ($req, $res, $args = []) {
 
    $params = $req->getParams();

    if (empty($params['q'])) {
        return $res->withStatus(400)->withJson([]);
    } 

    $pdo = $this->get('pdo');
    $query = "%{$params['q']}%";
    $stmt = $pdo->prepare('SELECT * FROM locations WHERE postcode like :query'); 
    $stmt->bindValue(':query', $query);
    $stmt->execute();
    $locations = $stmt->fetchAll(PDO::FETCH_ASSOC);
    

    return $res->withStatus(200)->withJson($locations);
});

$app->run();
